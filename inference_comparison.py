import numpy as np
import cv2
import os
import json
from tqdm import tqdm
from glob import glob
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras import layers, models, optimizers
from tensorflow.keras.callbacks import LearningRateScheduler
from custom_layers import yolov4_neck, yolov4_head, nms
from utils import load_weights, get_detection_data, draw_bbox, voc_ap, draw_plot_func, read_txt_to_list
from config import yolo_config
# from loss import yolo_loss
from tensorflow.keras import models
import time
import PIL.Image as Image
from skimage import transform
from keras.preprocessing import image

t1 = time.time()

#CONSTANTS 
img_size = (416, 416, 3)
num_classes = 4

anchors = np.array([12, 16, 19, 36, 40, 28, 36, 75, 76, 55, 72, 146, 142, 110, 192, 243, 459, 401]).reshape((3, 3, 2))
xyscale = [1.2, 1.1, 1.05]
null_array = [0. , 0. , 0. , 0. ]

max_boxes = 100
cfg_iou_threshold = 0.413
cfg_score_threshold = 0.5
FLAG = 0

class_names = ["class 1 left","class 1 right","class 2 left","class 2 right"]
class_color = {name: list(np.random.random(size=3)*255) for name in class_names}

#------------------------------------- MODEL LOADING -------------------------------------------------


#Model Loading
yolo_model = models.load_model("models/Check_3" , compile=False)
yolov4_output = yolov4_head(yolo_model.output, num_classes, anchors, xyscale)
inference_model = models.Model(yolo_model.input, nms(yolov4_output, img_size, num_classes , iou_threshold=cfg_iou_threshold, score_threshold=cfg_score_threshold)) 

upper = tf.keras.models.load_model("models/upper.h5")
lower = tf.keras.models.load_model("models/lower.h5")


arr = os.listdir("test_img")
print(len(arr))

#------------------------------------- MODEL INFERENCE -----------------------------------------------

for engine in arr:
  cam_img = os.listdir('test_img/'+engine)

  if len(cam_img) < 12:
    print("USELESSS FOLDER ......")
    break 

  upper_input = []
  lower_input = []

  for image in cam_img:
    raw_img = cv2.imread('test_img/'+engine+"/"+image)
    h, w = raw_img.shape[:2]

    img = cv2.resize(raw_img, (416,416))
    img = img / 255.
    imgs = np.expand_dims(img, axis=0)

    history = inference_model.predict(imgs)

    boxes = history[0][0][:4]
    accuracy = history[1][0][:4]
    labels = history[2][0][:4]

    check_all_zero = null_array in boxes

    if check_all_zero :
      print(engine," ",image," FAIL Empty Boxes")
      FLAG = 1
      break

    for (label , box) in zip(labels,boxes):

      cropped = raw_img[int(box[1] * h):int(box[3] * h), int(box[0] * w):int(box[2] * w), :].copy()
      np_image = np.array(cropped).astype('float32')/255
      np_image = transform.resize(np_image, (96, 96, 3))
      np_image = np.expand_dims(np_image, axis=0)

      if label==0 or label==1 :
        upper_input.append(np_image)
      else:
        lower_input.append(np_image)

    # break

  upper_input = np.vstack(upper_input)
  lower_input = np.vstack(lower_input)

  y_pred= upper.predict(upper_input)
  predicted_values_upper = np.argmax(y_pred, axis=1)
  # print(predicted_values)


  if (1 in predicted_values_upper):
    print(predicted_values_upper)
    print(engine," Upper Defective")

  y_pred = lower.predict(lower_input)
  predicted_values_lower = np.argmax(y_pred, axis=1)
  
  if (1 in predicted_values_lower):
    print(predicted_values_lower)
    print(engine," Lower Defective")

  print("TOTAL BOXES GENERATED : ",len(predicted_values_lower)+len(predicted_values_upper))
  
  # break

  if FLAG == 0:
    FLAG = 0
